use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value}; // 导入 serde_json 而不是 json

#[derive(Deserialize)]
struct Input {
    numbers: Vec<i32>,
}

#[derive(Serialize)]
struct Output {
    sorted_numbers: Vec<i32>,
}

async fn rank_score_handler(event: LambdaEvent<Input>) -> Result<Value, Error> {
    let (event, _) = event.into_parts();
    let mut numbers = event.numbers;
    numbers.sort_unstable_by(|a, b| b.cmp(a));  // 降序排序
    Ok(json!(Output { sorted_numbers: numbers }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(rank_score_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}