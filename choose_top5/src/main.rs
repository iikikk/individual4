use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value}; // Correctly import serde_json

#[derive(Deserialize)]
struct Input {
    sorted_numbers: Vec<i32>,
}

#[derive(Serialize)]
struct Output {
    top_five: Vec<i32>,
}

async fn choose_top5_handler(event: LambdaEvent<Input>) -> Result<Value, Error> {
    let (event, _) = event.into_parts();
    let top_five = event.sorted_numbers.iter().take(5).cloned().collect::<Vec<_>>();
    println!("Top 5 numbers: {:?}", top_five);
    Ok(json!(Output { top_five }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(choose_top5_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}