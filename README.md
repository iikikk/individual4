# Individual Project 4


## Requirments
Rust AWS Lambda function
Step Functions workflow coordinating Lambdas
Orchestrate data processing pipeline

## Steps:

### Rust AWS Lambda function

1. cargo lambda new choose_top5
cargo lambda new rank_score
2. Add dependencies to Cargo.toml.
3. Modify main.rs.
4. Create a new role with policies AWSLambda_FullAccess, AWSLambdaBasicExecutionRole, IAMFullAccess.
5. Build lambda function:
cargo lambda build --release
6. Deploy on AWS Lambda with:
cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>

### Step Functions workflow coordinating Lambdas/ Orchestrate data processing pipeline

1. Create a new State Machine
2. Implement it with code:
{
  "Comment": "A Step Functions state machine that orchestrates two Lambda functions.",
  "StartAt": "RankScore",
  "States": {
    "RankScore": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:REGION:ACCOUNT_ID:function:rank_score",
      "Next": "ChooseTop5"
    },
    "ChooseTop5": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:REGION:ACCOUNT_ID:function:choose_top5",
      "End": true
    }
  }
}
3. Testing with the following json:
{
    "numbers": [23, 1, 45, 12, 78, 34, 56, 13]
}


## Screenshot:
rank_score
 ![Alt text](image.png)
choose_top5
 ![Alt text](image-1.png)
 ![Alt text](image-2.png)
Execution Events
 ![Alt text](image-3.png)
Execution Status
 ![Alt text](image-4.png)
Execution input and output
 ![Alt text](image-5.png)
Graph View of Step Function
 ![Alt text](image-6.png)
